# q

```
^
|
Working title
```

## Install and run

* (Optionally) create virtual Python environment
* `pip install -r requirements.txt`
* Create `instance/config.py` and configure,
  see `q/default_settings.py` for details.
* [Set environment variables](http://flask.pocoo.org/docs/1.0/tutorial/factory/?highlight=env#run-the-application)
    * `FLASK_APP=q`
    * `FLASK_ENV=development`
* `flask run`

## Test

To run the unit tests, execute from the root folder: `python -m unittest`

## Persistence

The default configuration does not use a database but uses server side sessions
for sensitive session data, using `Flask-Session`. The sessions are stored in
files in the instance folder.

## Todo

* Switch over from OpenID Connect claims to directly using LDAP for displaying
the attributes of users (for simplicity).