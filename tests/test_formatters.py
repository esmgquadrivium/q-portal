from datetime import datetime, timezone, timedelta
from unittest import TestCase

from q.formatters import _parse_ldap_generalized_time


class LDAPGeneralizedTimeParserTestCase(TestCase):
    def test_z(self):
        val = "2018012603Z"
        expect = datetime(2018, 1, 26, 3, tzinfo=timezone.utc)
        result = _parse_ldap_generalized_time(val)
        self.assertEqual(expect, result)

    def test_differential_plus_hour(self):
        val = "2019120123+10"
        expect = datetime(2019, 12, 1, 23, tzinfo=timezone(timedelta(hours=10)))
        result = _parse_ldap_generalized_time(val)
        self.assertEqual(expect, result)

    def test_differential_minus_minute(self):
        val = "1999090909-0155"
        expect = datetime(1999, 9, 9, 9, tzinfo=timezone(-timedelta(hours=1, minutes=55)))
        result = _parse_ldap_generalized_time(val)
        self.assertEqual(expect, result)

    def test_minute(self):
        val = "123401230123Z"
        expect = datetime(1234, 1, 23, 1, 23, tzinfo=timezone.utc)
        result = _parse_ldap_generalized_time(val)
        self.assertEqual(expect, result)

    def test_second(self):
        val = "12340123012359Z"
        expect = datetime(1234, 1, 23, 1, 23, 59, tzinfo=timezone.utc)
        result = _parse_ldap_generalized_time(val)
        self.assertEqual(expect, result)
