"""
Formatters for OpenID Connect values
"""
import re
from datetime import date, datetime, timezone, timedelta


def address_formatter(address):
    keys = ['street_address', 'postal_code', 'locality', 'country']
    components = []
    for key in keys:
        if key in address:
            components.append(address[key])
    return ', '.join(components)


def date_formatter(d):
    """
    For dates in the format 20120326.
    """
    d = date(int(d[0:4]), int(d[4:6]), int(d[6:8]))
    return d.strftime("%x")


def _parse_ldap_generalized_time(time):
    """
    Parser for LDAP Generalized Time. Fractions are not supported!

    References:
    * https://tools.ietf.org/html/rfc4517#section-3.3.13
    * https://docs.python.org/3/library/datetime.html?highlight=date#datetime.datetime
    """
    # Build regex pattern
    year = '(?P<year>\d{4})'
    month = '(?P<month>\d{2})'
    day = '(?P<day>\d{2})'
    hour = '(?P<hour>\d{2})'
    minute = '(?P<minute>\d{2})?'
    second = '(?P<second>\d{2})?'
    fraction = '([.,](?P<fraction>\d))?'
    zone = '(?P<timezone>Z|([+-]\d{2}(\d{2})?))'
    # Do regex
    m = re.search('^' + year + month + day + hour + minute + second + fraction + zone + '$', time)
    groups = m.groupdict()
    if groups['fraction']:
        raise ValueError("LDAP Generalized Time fractions can't be parsed.")
    # Get components
    year = int(groups['year'])
    month = int(groups['month'])
    day = int(groups['day'])
    hour = int(groups['hour'])
    minute = int(groups['minute']) if groups['minute'] else 0
    second = int(groups['second']) if groups['second'] else 0
    # Leap second is not supported in Python < 3.6
    if second == 60:
        second = 59
    # Timezone
    zone = groups['timezone']
    if zone == 'Z':
        zone = timezone.utc
    else:
        zone_sign = -1 if zone[0] == '-' else 1
        zone_hour = int(zone[1:3])
        zone_minute = int(zone[3:5]) if len(zone) == 5 else 0
        zone = timezone(zone_sign * timedelta(hours=zone_hour, minutes=zone_minute))
    # Create datetime
    result = datetime(year, month, day, hour, minute, second, tzinfo=zone)
    return result


def ldap_generalized_time_formatter(time):
    time = _parse_ldap_generalized_time(time)
    return time.strftime("%c")


def boolean_formatter(b):
    """
    Returns Yes or No.
    """
    return "Yes" if b else "No"