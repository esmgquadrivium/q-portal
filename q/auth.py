import functools

from authlib.flask.client import OAuth
from authlib.specs.rfc7519 import JWT
from flask import Blueprint, redirect, session, url_for, g, current_app

bp = Blueprint('auth', __name__, url_prefix='/auth')

"""
This module provides g.user_claims and @login_required.
"""


def login_required(view):
    """View decorator that redirects anonymous users to the login page."""

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user_claims is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    """
    If a user id is stored in the session, load the user object from
    the database into ``g.user``.
    """
    id_token = session.get('id_token')

    if id_token is None:
        g.user_claims = None
    else:
        jwt = JWT()
        g.user_claims = jwt.decode(id_token, current_app.config['JWKS'])


_oauth = None


def init(app):
    """
    Initializes this module, needs to be called when creating the app.
    """
    global _oauth
    _oauth = OAuth(app)

    def _fetch_session_token():
        raise NotImplementedError()
        return session.get('token', None)

    def _update_session_token(token):
        raise NotImplementedError()
        session['token'] = token
        return token

    _oauth.register(
        name='q',
        fetch_token=_fetch_session_token,
        update_token=_update_session_token)


@bp.route('/login')
def login():
    redirect_uri = url_for('.authorize', _external=True)
    return _oauth.q.authorize_redirect(redirect_uri)


@bp.route('/authorize')
def authorize():
    token = _oauth.q.authorize_access_token()
    # Extract and store id_token
    session['id_token'] = token['id_token']
    return redirect(url_for('main.index'))


@bp.route('/logout')
def logout():
    """Clear the current session, including the stored user id."""
    # Todo
    session.clear()
    return redirect(url_for('main.index'))
