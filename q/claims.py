from flask import Blueprint, g, render_template, redirect, url_for, current_app
from .auth import login_required

bp = Blueprint('main', __name__)


class Claim():
    """
    Definition of an OAuth claim.
    """
    def __init__(self, name, friendly_name, formatter=None, help_text=None):
        self.name = name
        self.friendly_name = friendly_name
        self.formatter = formatter
        self.help_text = help_text

    def format_value(self, v):
        if self.formatter:
            return self.formatter(v)
        else:
            return v


@bp.route('/')
@login_required
def index():
    return redirect(url_for('.user_claims'))


@bp.route('/you')
@login_required
def user_claims():
    """
    Displays the user claims.
    """
    # Render claims according to configuration
    grouped_claim_values = []
    for group, claims in current_app.config['USER_CLAIMS']:
        claim_values = []
        for claim in claims:
            if claim.name in g.user_claims:
                value = claim.format_value(g.user_claims[claim.name])
                claim_values.append((claim, value))
        grouped_claim_values.append((group, claim_values))

    # claims = g.user_claims.items()

    return render_template('user_claims.html', grouped_claim_values=grouped_claim_values)
