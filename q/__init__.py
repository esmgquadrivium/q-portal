import os

from flask import Flask
from flask_session import Session
import os.path


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('q.default_settings')

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Initialize Flask-Session
    app.config.setdefault('SESSION_FILE_DIR',
                          os.path.join(app.instance_path, 'flask_session'))
    Session(app)

    from . import auth, claims

    # Auth module
    auth.init(app)
    app.register_blueprint(auth.bp)

    # Main module
    app.register_blueprint(claims.bp)

    return app
