from .formatters import address_formatter, date_formatter, ldap_generalized_time_formatter
from .claims import Claim

# Secret key, change this in production
# http://flask.pocoo.org/docs/1.0/api/?highlight=secret_key#flask.Flask.secret_key
SECRET_KEY = 'dev'

# OAuth configuration
# https://docs.authlib.org/en/latest/client/flask.html#configuration
Q_CLIENT_ID = None
Q_CLIENT_SECRET = None
# Get these from .well-known/openid-configuration
Q_AUTHORIZE_URL = None
Q_ACCESS_TOKEN_URL = None
Q_REFRESH_TOKEN_URL = None
Q_CLIENT_KWARGS = {'scope': 'openid email profile address phone'}
JWKS = None

# Session storage configuration
# https://pythonhosted.org/Flask-Session/
# Filesystem stores the sessions under the instance folder
SESSION_TYPE = 'filesystem'

# LDAP
LDAP_SERVER = None
LDAP_READ_DN = None
LDAP_READ_PASS = None

# Defines the layout of the displayed user attributes. The format of the setting is:
# [(Group name, [Claim, Claim, ...]), ...]
USER_CLAIMS = [
    ('Profile', [
        Claim('preferred_username', 'Username'),
        Claim('name', 'Name'),
        Claim('gender', 'Gender'),
        Claim('email', 'E-mail'),
        Claim('phone_number', 'Phone number'),
        Claim('address', 'Address', formatter=address_formatter),
        Claim('birthdate', 'Birthdate', formatter=date_formatter),
        Claim('locale', 'Locale'),
        Claim('updated_at', 'Last updated at', formatter=ldap_generalized_time_formatter),
    ]),
]
